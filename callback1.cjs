//	Problem 1: Write a function that will return a particular board's information based on the boardID that is passed from the given list of boards in boards.json and then pass control back to the code that called it by using a callback function.
const data = require("./data/boards_2.json");
function boardID(id, callback) {
  try {
    if (id == undefined || callback == undefined) {
      throw new Error("Data is not exported correctly");
    } else {
      setTimeout(() => {
        data.forEach((current) => {
          //console.log(current)
          if (current["id"] == id) {
            callback(null, current);
          }
        });
      }, 2 * 1000);
    }
  } catch (error) {
    console.log(error.message);
  }
}
module.exports = boardID;
