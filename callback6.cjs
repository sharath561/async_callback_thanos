/* 
	Problem 6: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for all lists simultaneously
*/

const boardData = require("./data/boards_2.json");

const boardID = require("./callback1.cjs");
const fecthListData = require("./callback2.cjs");
const fecthCardData = require("./callback3.cjs");

function ThanosAllData() {
  try {
    if (boardData == undefined) {
      throw new Error("Data is not exported correctly");
    } else {
      const thanosId = boardData.filter((current) => {
        if (current["name"] == "Thanos") {
          return current;
        }
      });
      //console.log(thanosId)

      const thanosDataId = thanosId[0]["id"];

      boardID(thanosDataId, (error, data) => {
        if (error) {
          console.log(error);
        } else {
          console.log(
            "---------- printing the Thanos board data -------------"
          );
          console.log(data);

          fecthListData(thanosDataId, (error, data) => {
            if (error) {
              console.log(error);
            } else {
              console.log(
                "---------- printing the Thanos list data ------------"
              );
              console.log(data);

              let traverseListData = data.forEach((names) => {
                //console.log(names["name"])
                fecthCardData(names["id"], (error, data) => {
                  if (error) {
                    console.log(error);
                  } else {
                    console.log(
                      `------- printing the Thanos ${names["name"]} stone data ----------`
                    );
                    console.log(data);
                  }
                });
              });
            }
          });
        }
      });
    }
  } catch (error) {
    console.log(error.message);
  }
}
module.exports = ThanosAllData;
//ThonasAllData();
