//Problem 2: Write a function that will return all lists that belong to a board based on the boardID that is passed to it from the given data in lists.json. Then pass control back to the code that called it by using a callback function.
const listData = require("./data/lists_1.json");
function fecthListData(id, callback) {
  try {
    if (id == undefined || callback == undefined) {
      throw new Error("Data is not exported correctly");
    } else {
      setTimeout(() => {
        callback(null, listData[id]);
      }, 2 * 1000);
    }
  } catch (error) {
    console.log(error.message);
  }
}
module.exports = fecthListData;
