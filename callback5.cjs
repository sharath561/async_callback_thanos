/* 
	Problem 5: Write a function that will use the previously written functions to get the following information. You do not need to pass control back to the code that called it.

    Get information from the Thanos boards
    Get all the lists for the Thanos board
    Get all cards for the Mind and Space lists simultaneously
*/
const boardID = require("./callback1.cjs");
const fecthListData = require("./callback2.cjs");
const fecthCardData = require("./callback3.cjs");

const boardData = require("./data/boards_2.json");

function previouslyFunctionsDataThanos() {
  try {
    if (boardData == undefined) {
      throw new Error("Data is not exported correctly");
    } else {
      const thanosData = boardData.filter((current) => {
        if (current["name"] == "Thanos") {
          return current;
        }
      });
      let thanosId = thanosData[0]["id"];

      boardID(thanosId, (error, data) => {
        if (error) {
          console.log(error);
        } else {
          console.log("--------printing Thanos boards data-----------");
          console.log(data);

          fecthListData(thanosId, (error, data) => {
            if (error) {
              console.log(error);
            } else {
              console.log(
                "-------------printing Thanos lists Data---------------"
              );
              console.log(data);

              let mindData = data.filter((current) => {
                if (current["name"] == "Mind") {
                  return current;
                } else if (current["name"] == "Space") {
                  return current;
                }
              });

              mindData.forEach((currentStones) => {
                //console.log(currentStones)
                fecthCardData(currentStones["id"], (error, data) => {
                  if (error) {
                    console.log(error);
                  } else {
                    console.log(
                      `---------printing the Thanos ${currentStones["name"]} stone data---------`
                    );
                    console.log(data);
                  }
                });
              });
            }
          });
        }
      });
    }
  } catch (error) {
    console.log(error.message);
  }
}
//previouslyFunctionsDataThanos();
module.exports = previouslyFunctionsDataThanos;
